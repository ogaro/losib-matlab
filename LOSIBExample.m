% Example of usage of LOSIB method.
% Developed by: Oscar Garc�a-Olalla (ogaro@unileon.es)
% VARP Group (http://pitia.unileon.es/VARP)
% University of Le�n

%Load images
img1=imread('test1.jpg');
img2=imread('test2.jpg');

%Extract LOSIB.
Losib1=getLosib(img1); %By default use radius=1 and neighbors=8
Losib2=getLosib(img2,1,8);
Losib3=getLosib(imread('test3.jpg')); %You can load the image directly in the method.

%Display the LOSIB values for the three images of test.
plot(Losib1,'-*r');hold all;plot(Losib2,'-+g');plot(Losib3,'-ob');legend('Losib1','Losib2','Losib3');hold off;

