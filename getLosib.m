function losib=getLosib(image,radius,neighbors)
% Code of the Local Oriented Statistic Information Booster (LOSIB), for
% texture retrieval.
% 
% function losib=getLosib(image,radius,neighbors)
% Neighbors extraction is based on the code LBP developed by Marko Heikkil�
% and Timo Ahonen.
%
% Parameters:
%  -image: Matrix with the image data.
%  -radius: An integer which determines the size of the neighborhood.
%  -neighbors: Number of orientations taken into account.
%
% Examples:
%   img=imread('imgTest.png');
%   Losib= getLosib(img); //Uses radius=1 and neighbors=8 by default.
%   ------------------------------
%   Losib=getLosib(imread('imgTest.png',2,16);
%   ------------------------------
% Modified:27/11/14 Revision: 1.0 
% Developed by: Oscar Garc�a-Olalla (ogaro@unileon.es)
% VARP Group (http://pitia.unileon.es/VARP)
% University of Le�n
% Contact email: ogaro@unileon.es
%
% If you use this method, please cite us:
%   Garc�a-Olalla, O., E. Alegre, L. Fern�ndez-Robles, and V. Gonz�lez-Castro, 
%   "Local Oriented Statistics Information Booster (LOSIB) for Texture Classification", 
%   International Conference on Pattern Recognition (ICPR), Stockholm, Sweden, 2014

if nargin<3
    radius=1;
    neighbors=8;
end
%This method implements the losib
neighborPoints=zeros(neighbors,2);%Initializate the variable that holds the neighbours coordinates.
angle=2*pi/neighbors; %Angle between each neighbor.
%Determine the coordinates of the neighbors given their angle and the
%radius.
for i = 1:neighbors
    neighborPoints(i,1) = -radius*sin((i-1)*angle);
    neighborPoints(i,2) = radius*cos((i-1)*angle);
end
if (size(image,3)==3)
    image=rgb2gray(image);%Image to gray.
    
end
image=im2double(image);
[numRows,numCols]=size(image);%Dimensions of the image
minR=-radius;%minimum value left of the central pixel.
maxR=radius; %aximum value right of the central pixel.
minC=-radius; %minimum value up of the central pixel.
maxC=radius;%maximum value down of the central pixel.

numRowsPatch=radius*2+1; %--radius--C--radius--
numColsPatch=radius*2+1; %--radius--C--radius--

%Determine the first valid pixel (not exceeds the limits of the image).
originRow=1+radius;% The first pixel must be at Radius pixels of the upper edge.
originCol=1+radius;% Equally, the first pixel must be at Radius pixels of the left edge.

dRows=numRows-numRowsPatch; %Number of rows computed (all except the border)
dCols=numCols-numColsPatch;

imageCenters= image(originRow:originRow+dRows, originCol:originCol+dCols);%image with the centers of the pixels.

for i = 1:neighbors
    row = neighborPoints(i,1)+originRow;%take the row and column for the neighbour i at the origin pixel.
    col = neighborPoints(i,2)+originCol;
    % Calculate floors, ceils and rounds for the x and y.
    floorRow = floor(row); ceilRow = ceil(row); roundRow = round(row);%extract the ceil, floor and rounded value of the row and column
    floorCol = floor(col); ceilCol = ceil(col); roundCol = round(col);
    
    %Check if interpolation is needed
    if (abs(row - roundRow) == 0) && (abs(col - roundCol) ==0)
        % Interpolation is not needed, use original datatypes
        N = image(roundRow:roundRow+dRows,roundCol:roundCol+dCols); %Select a matrix with upper left corner the pixel of the neighbour and dimension dRows X dCols
        % convert the matrix to a column vector
        NArray(:,i) = N(:); %Insert it matrix into a NArray in a column vector.
    else
        % Interpolation needed, use double type images
        ty = row - floorRow; %Get just the decimal part.
        tx = col - floorCol; % ''   ''  ''  ''  ''  ''
        
        % Calculate the interpolation weights.
        w1 = (1 - tx) * (1 - ty);
        w2 =      tx  * (1 - ty);
        w3 = (1 - tx) *      ty ;
        w4 =      tx  *      ty ;
        % Compute interpolated pixel values
        N = w1*image(floorRow:floorRow+dRows,floorCol:floorCol+dCols) + w2*image(floorRow:floorRow+dRows,ceilCol:ceilCol+dCols) + ...
            w3*image(ceilRow:ceilRow+dRows,floorCol:floorCol+dCols) + w4*image(ceilRow:ceilRow+dRows,ceilCol:ceilCol+dCols);% Sum the matrix of the four points nearer the interpolated point multiplying them by the factor w.
        
        % convert the matrix to a column vector
        NArray(:,i) = N(:);
    end
end
NArray(:,neighbors+1) = imageCenters(:); %In the last column, we allocate the image with all the centers.

for i=1:neighbors
    difference = abs(NArray(:,i)-NArray(:,neighbors+1));%*Calculate the difference between the central pixel and the corresponding neighbour for all the pixels in the image. MAGIC!
    losib(i) = mean(difference);    
end
end