**MATLAB Code for LOSIB method published in ICPR 2014 .
**

This repository contains two .m files:
1. getLosib.m (LOSIB method function)
2. LOSIBExample.m (Example of usage)

and three test images  test[1:3].jpg
 

 If you use this code, please cite us: 

*García-Olalla, O., E. Alegre, L. Fernández-Robles, and V. González-Castro, "Local Oriented Statistics Information Booster (LOSIB) for Texture Classification", International Conference on Pattern Recognition (ICPR), Stockholm, Sweden, 2014*

For any doubt, contact me at ogaro@unileon.es

Thank you.